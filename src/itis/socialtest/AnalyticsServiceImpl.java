package itis.socialtest;

import itis.socialtest.entities.Author;
import itis.socialtest.entities.Post;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class AnalyticsServiceImpl implements AnalyticsService {
    @Override
    public List<Post> findPostsByDate(List<Post> posts, String date) {
        return posts.stream()
                .filter(post -> post.getDate().equals(date))
                .collect(Collectors.toList());
    }

    @Override
    public String findMostPopularAuthorNickname(List<Post> posts) {
//        Map<Author, Long> collect = posts.stream()
//                .map(Post::getAuthor)
//                .distinct()
//                .collect(
//                        Collectors.toMap(Function.identity(),
//                                author -> posts.stream()
//                                        .filter(post -> post.getAuthor().
//                                                equals(author))
//                                        .map(Post::getLikesCount)
//                                        .reduce(Long::sum).get()));
        return null;
    }

    @Override
    public Boolean checkPostsThatContainsSearchString(List<Post> posts, String searchString) {
        return posts.stream()
                .anyMatch(post -> post.getContent()
                        .contains(searchString));
    }

    @Override
    public List<Post> findAllPostsByAuthorNickname(List<Post> posts, String nick) {


        return posts.stream()
                .filter(post -> post.getAuthor().getNickname()
                        .equals(nick))
                .collect(Collectors.toList());
    }
}
