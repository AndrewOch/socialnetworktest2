package itis.socialtest;


import itis.socialtest.entities.Author;
import itis.socialtest.entities.Post;

import java.io.*;
import java.util.ArrayList;
import java.util.List;


/*
 * В папке resources находятся два .csv файла.
 * Один содержит данные о постах в соцсети в следующем формате: Id автора, число лайков, дата, текст
 * Второй содержит данные о пользователях  - id, никнейм и дату рождения
 *
 * Напишите код, который превратит содержимое файлов в обьекты в package "entities"
 * и осуществите над ними следующие опреации:
 *
 * 1. Выведите в консоль все посты в читабельном виде, с информацией об авторе.
 * 2. Выведите все посты за сегодняшнюю дату
 * 3. Выведите все посты автора с ником "varlamov"
 * 4. Проверьте, содержит ли текст хотя бы одного поста слово "Россия"
 * 5. Выведите никнейм самого популярного автора (определять по сумме лайков на всех постах)
 *
 * Для выполнения заданий 2-5 используйте методы класса AnalyticsServiceImpl (которые вам нужно реализовать).
 *
 * Требования к реализации: все методы в AnalyticsService должны быть реализованы с использованием StreamApi.
 * Использование обычных циклов и дополнительных переменных приведет к снижению баллов, но допустимо.
 * Парсинг файлов и реализация методов оцениваются ОТДЕЛЬНО
 *
 *
 * */

public class MainClass {

    private List<Post> allPosts;

    private AnalyticsService analyticsService = new AnalyticsServiceImpl();

    public static void main(String[] args) {
        new MainClass().run("src\\itis\\socialtest\\resources\\PostDatabase.csv",
                "src\\itis\\socialtest\\resources\\Authors.csv");
    }

    private void run(String postsSourcePath, String authorsSourcePath) {
        allPosts = new ArrayList<>();
        File postsFile = new File(postsSourcePath);
        File authorsFile = new File(authorsSourcePath);

        try {
            FileReader fileReader = new FileReader(authorsFile);
            BufferedReader bufferedReader = new BufferedReader(fileReader);


            List<Author> authors = new ArrayList<>();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                Long id = Long.valueOf(line.substring(0, line.indexOf(',')));
                line = line.substring(line.indexOf(',') + 2);
                String nickname = line.substring(0, line.indexOf(','));
                String birthdayDate = line.substring(line.indexOf(',') + 2);

                authors.add(new Author(id, nickname, birthdayDate));
            }
            fileReader = new FileReader(postsFile);
            bufferedReader = new BufferedReader(fileReader);
            while ((line = bufferedReader.readLine()) != null) {
                Long id = Long.valueOf(line.substring(0, line.indexOf(',')));
                Author author = authors.stream()
                        .filter(author1 ->
                                author1.getId().equals(id)).findFirst().orElse(null);

                line = line.substring(line.indexOf(',') + 2);

                Long likes = Long.valueOf(line.substring(0, line.indexOf(',')));
                line = line.substring(line.indexOf(',') + 2);

                String date = line.substring(0, line.indexOf('T'));
                line = line.substring(line.indexOf(',') + 2);

                String content = line;

                allPosts.add(new Post(date, content, likes, author));
            }

            System.out.println(allPosts);

            System.out.println("Today's posts:");
            System.out.println(analyticsService.findPostsByDate(allPosts, "17.04.2021"));

            System.out.println("Posts of varlamov:");
            System.out.println(analyticsService.findAllPostsByAuthorNickname(allPosts, "varlamov"));

            System.out.print("Posts contain word Россия: ");
            System.out.println(analyticsService.checkPostsThatContainsSearchString(allPosts, "Россия"));

            System.out.println("Most popular author:");
            System.out.println(analyticsService.findMostPopularAuthorNickname(allPosts));

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
